Source: censys-python
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
	       python3-backoff,
	       python3-parameterized,
	       python3-pytest,
	       python3-pytest-cov,
	       python3-requests-mock,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-copybutton,
               python3-sphinx-rtd-theme,
               python3-sphinx-tabs,
               python3-sphinxcontrib.autoprogram
Standards-Version: 4.5.1
Homepage: https://github.com/censys/censys-python
Vcs-Browser: https://gitlab.com/kalilinux/packages/censys-python
Vcs-Git: https://gitlab.com/kalilinux/packages/censys-python.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-censys
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-censys-doc
Description: lightweight API wrapper for the Censys Search Engine (Python 3)
 This package contains an easy-to-use and lightweight API wrapper for the
 Censys Search Engine (censys.io).
 .
 This package installs the library for Python 3.

Package: python-censys-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: lightweight API wrapper for the Censys Search Engine (common documentation)
 This package contains an easy-to-use and lightweight API wrapper for the
 Censys Search Engine (censys.io).
 .
 This is the common documentation package.
